module Queue exposing (getQueue, enqueue, dequeue, Queue, initialise)

import Array exposing (..)


type alias Queue a =
    { queue : Array a
    , front : Int
    , size : Int
    }


initialise : a -> Int -> Queue a
initialise t size =
    { queue = Array.initialize size (always t), front = 0, size = 0 }


enqueue : a -> Queue a -> Queue a
enqueue item queueI =
    if (queueI.size) /= Array.length queueI.queue then
        { queueI | queue = Array.set ((queueI.front + queueI.size) % Array.length queueI.queue) item queueI.queue, size = queueI.size + 1 }
    else
        queueI


dequeue : Queue a -> ( Maybe a, Queue a )
dequeue queue =
    if queue.size > 0 then
        ( Array.get queue.front queue.queue, { queue | front = queue.front + 1, size = queue.size - 1 } )
    else
        ( Nothing, queue )


getQueue : Queue a -> Array a
getQueue queue =
    queue.queue


size : Queue a -> Int
size queue =
    queue.size
