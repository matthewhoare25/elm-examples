module QueuesExample exposing (..)

import Html exposing (..)
import Queue exposing (..)
import Array exposing (..)
import Html.Events exposing (..)
import Html.Attributes exposing (..)


main =
    Html.program
        { view = view
        , update = update
        , init = initialState
        , subscriptions = subscriptions
        }


subscriptions model =
    Sub.none


view : Model -> Html Msg
view model =
    div []
        [ renderList (List.reverse (toList (Queue.getQueue model.queue)))
        , button [ onClick Pop ] [ text "Dequeue" ]
        , div [ attribute "role" "form" ] [ input [ id "input", type_ "text", value model.temp, onInput (\str -> Temp str) ] [], button [ onClick Push ] [ text "Enqueue" ] ]
        , text ("Latest Removed: " ++ (Maybe.withDefault "" model.popped))
        ]


renderList lst =
    ul []
        (List.map (\l -> li [] [ text l ]) lst)


toText string =
    div [] [ text string ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Pop ->
            let
                returnStack =
                    Queue.dequeue model.queue
            in
                ( { model | queue = Tuple.second returnStack, popped = Tuple.first returnStack }, Cmd.none )

        Push ->
            let
                returnStack =
                    Queue.enqueue model.temp model.queue
            in
                ( { model | queue = returnStack }, Cmd.none )

        Temp item ->
            ( { model | temp = item }, Cmd.none )


type alias Model =
    { queue : Queue String
    , popped : Maybe String
    , temp : String
    }


initialState : ( Model, Cmd Msg )
initialState =
    ( { temp = "", queue = Queue.initialise "" 10, popped = Nothing }
    , Cmd.none
    )


type Msg
    = Pop
    | Temp String
    | Push
