module Stack exposing (initialise, push, pop, Stack)

import Array exposing (..)


type alias Stack a =
    { data : Array a
    , top : Int
    , default : a
    }


initialise : a -> Int -> Stack a
initialise t size =
    { data = Array.initialize size (always t)
    , top = 0
    , default = t
    }


push : a -> Stack a -> Stack a
push item stack =
    if Array.length stack.data /= stack.top then
        { stack | data = Array.set stack.top item stack.data, top = stack.top + 1 }
    else
        stack


pop : Stack a -> ( Maybe a, Stack a )
pop stack =
    if stack.top == 0 then
        ( Nothing, stack )
    else
        let
            item =
                Array.get (stack.top - 1) stack.data
        in
            ( item, { stack | data = Array.set (stack.top - 1) stack.default stack.data, top = (stack.top - 1) } )
