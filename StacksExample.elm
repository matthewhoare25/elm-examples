module StacksExample exposing (..)

import Html exposing (..)
import Stack exposing (..)
import Array exposing (..)
import Html.Events exposing (..)
import Html.Attributes exposing (..)


main =
    Html.program
        { view = view
        , update = update
        , init = initialState
        , subscriptions = subscriptions
        }


subscriptions model =
    Sub.none


view : Model -> Html Msg
view model =
    div []
        [ renderList (List.reverse (toList model.stack.data))
        , button [ onClick Pop ] [ text "Pop" ]
        , div [ attribute "role" "form" ] [ input [ id "input", type_ "text", value model.temp, onInput (\str -> Temp str) ] [], button [ onClick Push ] [ text "Push" ] ]
        , text ("Popped: " ++ (Maybe.withDefault "" model.popped))
        ]


renderList lst =
    ul []
        (List.map (\l -> li [] [ text l ]) lst)


toText string =
    div [] [ text string ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Pop ->
            let
                returnStack =
                    Stack.pop model.stack
            in
                ( { model | stack = Tuple.second returnStack, popped = Tuple.first returnStack }, Cmd.none )

        Push ->
            let
                returnStack =
                    Stack.push model.temp model.stack
            in
                ( { model | stack = returnStack }, Cmd.none )

        Temp item ->
            ( { model | temp = item }, Cmd.none )


type alias Model =
    { stack : Stack String
    , popped : Maybe String
    , temp : String
    }


initialState : ( Model, Cmd Msg )
initialState =
    ( { temp = "", stack = Stack.initialise "" 10, popped = Nothing }
    , Cmd.none
    )


type Msg
    = Pop
    | Temp String
    | Push
