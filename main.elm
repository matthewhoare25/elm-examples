module Main exposing (..)

import TypesM exposing (Messages(..))
import Html


type Msg =
        TypesM.Sum
                |Other

update : Msg -> Int -> (Int, Cmd Msg)
update msg model = 
        case msg of 
                TypesM.Sum ->
                        (1 + 1, Cmd.none)
                _->
                        (model, Cmd.none)

run =
        update TypesM.Sum 0
